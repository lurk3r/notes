
注：这是本科第四年毕业论文读的内容
--

#Cauchy问题

1.Cauchy问题的唯一性以及稳定性

一般情况下稳定性的证明可以推导出唯一性。下面就普通的二阶的偏微分方程做一点讨论。

## 椭圆方程的Backward Cauchy问题

- 解法思路1：利用eigenfunction，数值上的解法需要解一系列ODE……

- 解法思路2：考虑解的L2范数的logarithm convexity……

- 解法思路3：利用半群……

## Carleman估计

## PDE方程组

###Maxwell‘s system

# 椭圆方程之单边界数据

# 椭圆方程之多边界数据

## DtN映射

## 系数恢复

# 积分几何与断层成像技术

## Radon变化以及其逆变化

## 传输方程